# Insecurity Debian docker base image #
Autobuilded `debian:stretch` up-to-date image

### About ###

Dockerfile based on official `debian` repository to build an up-to-date `debian:stretch` baseimage (apt-get dist-upgrade) with a couple of extra packages. 

The image is built on top of the most recently tagged `debian:stretch` image and installs the following extra packages:

 - ca-certificate
 - unzip
 - wget
 - vim-tiny
 - curl

When the official debian repository is updated, this image is automatically rebuilt. Moreover, a crontab job on my server checks 1x/day and rebuilds this image if updates are available.
 
Additionally apt is configured to NOT install `recommended` or `suggested` packages and with `Acquire::Retries` to 5 if a package download fails.
